#include "svd.h"
#include <iostream>
#include "omp.h"
#include "../headers/PerfEvent.hpp"
#include "../headers/aligned_allocator.h"


template <class T>
using aligned_vector = std::vector<T, alligned_allocator<T, 64>>;
using namespace std;

int main(){
    int n = 4;
    int m = 2;
    double in [] = {1,2,3,4,5,6,7,8};
/*
    int n = 3;
    int m = 3;
    double in [] = {12,22,33,44,55,66,77,88,99};
*/
    SVD *svd;
    auto start = omp_get_wtime();


//    PerfEvent e;
//    {
//        e.startCounters();
        svd = compute_svd(in, n, m, -1.0, 0.00001);
//        e.stopCounters();
//        e.printReport(std::cout, n); // use n as scale factor
//    }

    cout<<" Time Taken = "<< omp_get_wtime() - start<<"\n";


    int k = svd->k;
    m = svd->n;
    n = svd->m;
    const double * S = svd->s;
    const double *VT = svd->VT;
    const double * U = svd->U;

    cout<<"\n S   ";

    for (int i = 0 ; i< k ; i++)
        cout<<S[i]<<" ";

    cout<<"\n V   ";

    for (int i = 0 ; i< m*k ; i++)
        cout<<VT[i]<<" ";

    cout<<"\n U   ";

    for (int i = 0 ; i< k*n ; i++)
        cout<<U[i]<<" ";

    aligned_vector<double> first(n*m);//  = (double *) malloc(n*m* sizeof(double));

    reconstruct(svd, first.data());
    cout<<"\n Original   ";

    for (int i = 0 ; i< first.size() ; i++)
        cout<<first[i]<<" ";
    free_svd_result(svd);
    //free(first);
    return 0;

}
